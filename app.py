from flask import Flask,render_template,request,redirect,url_for
from flask_mysqldb import MySQL
import yaml
import socket
import platform

app = Flask(__name__)
#configure db
#db = yaml.load(open('db.yaml'))
#print ('this is db-------->>>>>>>>> ', db)
#app.config["MYSQL_HOST"] = db['mysql_host']
#app.config["MYSQL_USER"] = db['mysql_user']
#app.config["MYSQL_PASSWORD"] = db['mysql_password']
#app.config["MYSQL_DB"] = db['mysql_db']
#pass the app parameter into mysql module
#mysql=MySQL(app)

#getting the details
@app.route('/machineinfo')

def getEmployees():
    #get machine details
    host_name = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)
    os_name = platform.system()

    # get database records
#    cur=mysql.connection.cursor()
 #   result = cur.execute("select * from employees")
  #  if result > 0 :
   #     empDetails = cur.fetchall()

    return render_template("employees.html", host_name=host_name, host_ip=host_ip, os_name=os_name)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8088)

